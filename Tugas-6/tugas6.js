console.log("============");
console.log("== SOAL 1 ==");
console.log("============");

const pi = 3.14
let LuasLingkaran = (jarijari) => { return pi * jarijari * jarijari; };
console.log(LuasLingkaran(10));

const pi = 3.14
let KelilingLingkaran = (jarijari) => { return 2 * pi * jarijari; };
console.log(KelilingLingkaran(10));


console.log("============");
console.log("== SOAL 2 ==");
console.log("============");

function Kata() {
    const kata1 = 'saya'
    const kata2 = 'adalah'
    const kata3 = 'seorang'
    const kata4 = 'fronted'
    const kata5 = 'developer'

    const theString = `${kata1} ${kata2} ${kata3} ${kata4} ${kata5}`

    return theString;
}
let kalimat = Kata()
console.log(kalimat)

console.log("============");
console.log("== SOAL 3 ==");
console.log("============");

literal = (firstName, lastName) => {

    const theString = firstName + " " + lastName

    return theString;
}
console.log(literal('Wiliam', 'Imoh'))


console.log("============");
console.log("== SOAL 4 ==");
console.log("============");

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation, spell } = newObject
console.log(firstName, lastName, destination, occupation, spell)


console.log("============");
console.log("== SOAL 5 ==");
console.log("============");

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]

console.log(combined);